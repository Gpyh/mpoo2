package serie09;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EnumMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;
import java.util.Observer;

import serie09.SpeedometerModel.SpeedUnit;

/**
 * Created by gpyh on 11/24/15.
 */
public class Speedometer {

    private static final double STEP = 1;
    private static final double MAXSPEED = 100;

    private static final String TURN_MOTOR_ON_LABEL = "TURN ON";
    private static final String TURN_MOTOR_OFF_LABEL = "TURN OFF";

    private JFrame mainFrame;

    private Map<SpeedometerModel.SpeedUnit, JRadioButton> speedRadios;
    private JButton speedUpButton;
    private JButton slowDownButton;
    private JButton motorButton;
    private GraphicSpeedometer speedometerView;

    private SpeedometerModel model;

    public Speedometer() {
        createModel();
        createView();
        placeComponents();
        createController();
    }

    public void display() {
        refresh();
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    private void createModel () {
        this.model = new StdSpeedometerModel(STEP, MAXSPEED);
    }

    private void createView () {
        this.mainFrame = new JFrame();
        this.speedRadios = new EnumMap<SpeedUnit, JRadioButton>(SpeedUnit.class);
        for(SpeedUnit su : SpeedUnit.values()) {
            this.speedRadios.put(su, new JRadioButton(su.toString(), this.model.getUnit() == su));
        }
        this.slowDownButton = new JButton("-");
        this.speedUpButton = new JButton("+");
        this.motorButton = new JButton("PROUT");
        this.speedometerView = new GraphicSpeedometer(this.model);
    }

    private void placeComponents () {
        JPanel p = new JPanel(new GridLayout(0, 1)); {
            JPanel q = new JPanel(); {
                JPanel r = new JPanel(new GridLayout(0, 1)); {
                    for(JRadioButton radio : this.speedRadios.values()) {
                        r.add(radio);
                    }
                }
                q.add(r);
            }
            p.add(q);
            q = new JPanel(); {
                JPanel r = new JPanel(new GridLayout(1, 0)); {
                    r.add(this.slowDownButton);
                    r.add(this.speedUpButton);
                }
                q.add(r);
            }
            p.add(q);
            q = new JPanel(); {
                q.add(this.motorButton);
            }
            p.add(q);
        }
        mainFrame.add(p, BorderLayout.WEST);
        mainFrame.add(this.speedometerView, BorderLayout.CENTER);
    }

    private class SpeedUnitActionListener implements ActionListener {

        private SpeedUnit speedUnit;
        SpeedUnitActionListener(SpeedUnit speedUnit) {
            this.speedUnit = speedUnit;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            model.setUnit(this.speedUnit);
        }
    }

    private void createController () {
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        model.addObserver(new Observer() {
            public void update(Observable o, Object arg) {
                refresh();
            }
        });
        model.addObserver(this.speedometerView);

        ButtonGroup bg = new ButtonGroup();
        for(Entry<SpeedUnit, JRadioButton> entry : this.speedRadios.entrySet()) {
            bg.add(entry.getValue());
            entry.getValue().addActionListener(new SpeedUnitActionListener(entry.getKey()));
        }

        this.slowDownButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.slowDown();
            }
        });

        this.speedUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                model.speedUp();
            }
        });

        this.motorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(model.isOn()) {
                    model.turnOff();
                } else {
                    model.turnOn();
                }
            }
        });
    }

    private void refresh() {
        this.motorButton.setText(model.isOn() ? TURN_MOTOR_OFF_LABEL : TURN_MOTOR_ON_LABEL);
        this.slowDownButton.setEnabled(model.isOn());
        this.speedUpButton.setEnabled(model.isOn());
    }

    public static void main(String[] argv) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Speedometer().display();
            }
        });
    }
}
