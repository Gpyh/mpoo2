package serie09;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by gpyh on 11/24/15.
 */
public class GraphicSpeedometer extends JComponent implements Observer {

    // marge horizontale interne de part et d'autre du composant
    private static final int MARGIN = 40;
    // épaisseur de la ligne horizontale graduée
    private static final int THICK = 3;
    // demi-hauteur d'une graduation
    private static final int MARK = 5;
    // largeur de la base du triangle pour la tête de la flèche
    private static final int ARROW_BASE = 20;
    // épaisseur du corps de la flèche
    private static final int ARROW_THICK = 4;
    // hauteur du corps de la flèche
    private static final int ARROW_HEIGHT = 20;
    // hauteur préférée d'un GraphicSpeedometer
    private static final int PREFERRED_HEIGHT = 3 * (3 * MARK + ARROW_BASE / 2 + ARROW_HEIGHT);
    // facteur d'échelle pour l'espacement entre deux graduations
    private static final double ASPECT_RATIO = 1.25;
    // couleur bleu franc lorsque le moteur est allumé
    private static final Color BLUE = Color.BLUE;
    // couleur rouge franc lorsque le moteur est allumé
    private static final Color RED = Color.RED;
    // couleur bleu grisé lorsque le moteur est éteint
    private static final Color GRAYED_BLUE = new Color(0, 0, 255, 50);
    // couleur rouge grisé lorsque le moteur est éteint
    private static final Color GRAYED_RED = new Color(255, 0, 0, 50);
    // les vitesses affichées sont celles, entre 0 et model.getMaxSpeed(), qui sont les multiples de SPLIT_SIZE
    private static final int SPLIT_SIZE = 10;

    private SpeedometerModel model;

    private Graphics g;
    private int w;
    private int h;

    public GraphicSpeedometer(SpeedometerModel model) {
        super();
        this.model = model;
        this.setPreferredSize(new Dimension(
                PREFERRED_HEIGHT,
                (int) (ASPECT_RATIO * ARROW_BASE * (this.model.getMaxSpeed() / SPLIT_SIZE) + 2 * MARGIN)));
    }

    @Override
    public void update(Observable o, Object arg) {
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        this.g = g;
        this.w = getWidth() - 2*MARGIN;
        this.h = getHeight() / 3;
        g.setColor(model.isOn() ? RED : GRAYED_RED);
        paintArrow();
        g.setColor(model.isOn() ? BLUE : GRAYED_BLUE);
        paintRule();
    }

    private void paintArrow() {
        int x = (int) (MARGIN + w * model.getSpeed() / model.getMaxSpeed());
        int y = h + THICK + 2 * MARK;
        int[] xs = {x,
                x + ARROW_BASE / 2,
                x + ARROW_THICK / 2,
                x + ARROW_THICK / 2,
                x - ARROW_THICK / 2,
                x - ARROW_THICK / 2,
                x - ARROW_BASE / 2};
        int[] ys = {y,
                y + ARROW_BASE / 2,
                y + ARROW_BASE / 2,
                y + ARROW_BASE / 2 + ARROW_HEIGHT,
                y + ARROW_BASE / 2 + ARROW_HEIGHT,
                y + ARROW_BASE / 2,
                y + ARROW_BASE / 2};
        g.fillPolygon(xs, ys, 7);
    }

    private void paintRule() {
        FontMetrics fm = g.getFontMetrics();
        int maxSpeed = (int) model.getMaxSpeed();
        String speedString;
        int x;
        for(int speed = 0; speed <= maxSpeed; speed += SPLIT_SIZE) {
            speedString = String.valueOf(speed);
            x = MARGIN + w * speed / maxSpeed;
            g.drawLine(x, h - MARK, x, h + MARK);
            g.drawString(speedString, x - fm.stringWidth(speedString)/2, h - 2 * MARK);
        }
        g.fillRect(MARGIN, getHeight()/3, w, THICK);
    }


}
