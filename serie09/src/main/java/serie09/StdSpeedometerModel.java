package serie09;

import util.Contract;

import java.util.Observable;

public class StdSpeedometerModel extends Observable implements SpeedometerModel {

    private double maxSpeed;
    private double step;
    private SpeedUnit unit;

    private boolean on;
    private double speed;

    public StdSpeedometerModel(double step, double max) {
        Contract.checkCondition(step >= 1 && step <= max);
        this.maxSpeed = max;
        this.step = step;
        this.unit = SpeedUnit.KMH;
        this.on = false;
        this.speed = 0;
    }

    @Override
    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    @Override
    public double getSpeed() {
        return this.speed;
    }

    @Override
    public double getStep() {
        return this.step;
    }

    @Override
    public SpeedUnit getUnit() {
        return this.unit;
    }

    @Override
    public boolean isOn() {
        return this.on;
    }

    private double convert(double speed, SpeedUnit unit) {
        return speed * unit.getUnitPerKm() / this.unit.getUnitPerKm();
    }
    @Override
    public void setUnit(SpeedUnit unit) {
        Contract.checkCondition(unit != null);
        if(this.unit != unit) {
            this.speed = convert(this.speed, unit);
            this.step = convert(this.step, unit);
            this.maxSpeed = convert(this.maxSpeed, unit);
            this.unit = unit;
        }
        setChanged();
        notifyObservers();
        notifyObservers();
    }

    @Override
    public void slowDown() {
        Contract.checkCondition(this.on);
        this.speed -= this.step;
        if(this.speed < 0) {
            this.speed = 0;
        }
        setChanged();
        notifyObservers();
    }

    @Override
    public void speedUp() {
        Contract.checkCondition(this.on);
        this.speed += this.step;
        if(this.speed > this.maxSpeed) {
            this.speed = this.maxSpeed;
        }
        setChanged();
        notifyObservers();
    }

    @Override
    public void turnOff() {
        Contract.checkCondition(this.on);
        this.on = false;
        this.speed = 0;
        setChanged();
        notifyObservers();
    }

    @Override
    public void turnOn() {
        Contract.checkCondition(!this.on);
        this.on = true;
        setChanged();
        notifyObservers();
    }
}
