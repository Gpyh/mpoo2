package serie07;

import java.util.Arrays;
import java.util.Observable;
import java.util.Random;

import util.Contract;

public class StdSlotModel extends Observable implements SlotModel {
	
	private final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	private final int[] rewards;
	private final int nletters;
	
	private int gains;
	private int losses;
	private int payout;
	private String letters;
	
	public StdSlotModel(int[] rewards) {
		Contract.checkCondition(rewards.length > 1);
		Contract.checkCondition(rewards[0] == 0);
		this.rewards = rewards;
		this.nletters = rewards.length;
		this.gains = 0;
		this.losses = 0;
		char[] bytes = new char[this.nletters];
		Arrays.fill(bytes, ' ');
		this.letters = new String(bytes);
	}
	
	public StdSlotModel() {
		this(new int[] {0, 5, 300});
	}

	@Override
	public int lastPayout() {
		return this.payout;
	}

	@Override
	public int moneyLost() {
		return this.losses;
	}

	@Override
	public int moneyWon() {
		return this.gains;
	}

	@Override
	public String result() {
		return this.letters;
	}

	@Override
	public void gamble() {
		int[] occurences = new int[26];
		Random random = new Random();
		int rand;
		this.letters = "";
		for(int i = 0; i < this.nletters; i++) {
			rand = random.nextInt(26);
			// Faire un StringBuilder!
			this.letters+= ALPHABET.charAt(rand);
			occurences[rand]++;
		}
		int maxOccurence = 0;
		for(int occ : occurences) {
			if(occ > maxOccurence) {
				maxOccurence = occ;
			}
		}
		this.payout = rewards[maxOccurence-1];
		this.gains+= this.payout;
		this.losses++;
		setChanged();
		notifyObservers();
	}
}
