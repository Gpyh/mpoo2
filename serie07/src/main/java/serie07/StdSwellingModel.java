package serie07;

import java.awt.Dimension;
import java.util.Observable;

import util.Contract;

public class StdSwellingModel extends Observable implements SwellingModel {
	
	private Dimension minimalDimension;
	private Dimension dimension;
	
	public StdSwellingModel() {
		this.minimalDimension = new Dimension(DEFAULT_MINIMAL_DIM);
		this.dimension = new Dimension(this.minimalDimension);
	}

	@Override
	public Dimension getMinimalDimension() {
		return new Dimension(this.minimalDimension);
	}

	@Override
	public Dimension getDimension() {
		return new Dimension(this.dimension);
	}

	@Override
	public void setMinimalDimension(Dimension dim) {
		Contract.checkCondition(dim != null);
		Contract.checkCondition(dim.getWidth() >= 0 && dim.getWidth() <= MAXIMAL_DIM.getWidth());
		Contract.checkCondition(dim.getHeight() >= 0 && dim.getHeight() <= MAXIMAL_DIM.getHeight());
		this.dimension.setSize(
				Double.max(this.dimension.getWidth(), dim.getWidth()),
				Double.max(this.dimension.getHeight(), dim.getHeight())
			);
		this.minimalDimension.setSize(dim.getWidth(), dim.getHeight());
	}

	@Override
	public void scale(double factor) {
		Contract.checkCondition(factor >= -1);
		this.dimension.setSize(
				Double.min(
						Double.max(this.dimension.getWidth()*(1+factor), this.minimalDimension.getWidth()),
						MAXIMAL_DIM.getWidth()),
				Double.min(
						Double.max(this.dimension.getHeight()*(1+factor), this.minimalDimension.getHeight()),
						MAXIMAL_DIM.getHeight())
				);
	}

}
