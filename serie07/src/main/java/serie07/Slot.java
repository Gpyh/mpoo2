package serie07;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class Slot {

	private static final String APP_TITLE = "Slot Machine";
	private static final String PLAY_BUTTON_LABEL = "Tentez votre chance !";
	private static final String LOSSES_INFO = "Pertes : ";
	private static final String GAINS_INFO = "Gains : ";
	private static final String PAYOUT_INFO = "Vous venez de gagner : ";
	
	private SlotModel model;

	private JFrame mainFrame;
	private JTextField resultView;
	private JLabel lossesView;
	private JLabel gainsView;
	private JLabel payoutView;
	private JButton playButton;


    // CONSTRUCTEURS
    
    public Slot() {
        createModel();
        createView();
        placeComponents();
        createController();
    }
    
    public void display() {
        refresh();
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }
    
	private void createModel() {
    	this.model = new StdSlotModel();
    }
    
    private void createView() {
    	final int frameWidth = 300;
        final int frameHeight = 300;
        
        this.mainFrame = new JFrame(APP_TITLE);
        this.mainFrame.setPreferredSize(new Dimension(frameWidth, frameHeight));
        
        this.playButton = new JButton(PLAY_BUTTON_LABEL);
        
        this.resultView = new JTextField(3); {
        	this.resultView.setEditable(false);
        	this.resultView.setHorizontalAlignment(JTextField.CENTER);
        }
        
        this.lossesView = new JLabel("0");
        this.gainsView = new JLabel("0");
        this.payoutView = new JLabel("0");
		
	}

    private void createController() {
    	mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        model.addObserver(new Observer() {
            public void update(Observable o, Object arg) {
                refresh();
            }
        });

        this.playButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                model.gamble();
            }
        });
		
	}

	private void placeComponents() {
		JPanel p = new JPanel(); {
            p.add(this.playButton);
            p.add(this.resultView);
        }
        this.mainFrame.add(p, BorderLayout.NORTH);
		p = new JPanel(); {
			p.add(new JLabel(LOSSES_INFO));
			p.add(this.lossesView);
			p.add(new JLabel(GAINS_INFO));
			p.add(this.gainsView);
		}
        this.mainFrame.add(p, BorderLayout.CENTER);
        p = new JPanel(); {
        	p.add(new JLabel(PAYOUT_INFO));
        	p.add(this.payoutView);
        }
        this.mainFrame.add(p, BorderLayout.SOUTH);
	}
	
    private void refresh() {
		this.resultView.setText(this.model.result());
		this.lossesView.setText("" + this.model.moneyLost());
		this.gainsView.setText("" + this.model.moneyWon());
		this.payoutView.setText("" + this.model.lastPayout());
	}
    
    public static void main(String[] args) {
    	SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new Slot().display();
            }
        });
    }
    

	

	
}
