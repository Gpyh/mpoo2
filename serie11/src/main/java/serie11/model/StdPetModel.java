package serie11.model;

import java.io.File;
import java.util.Observable;
import java.util.Observer;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class StdPetModel extends Observable implements PetModel {
	
	private FilDocManager manager;
	
	public StdPetModel() {
		this.manager = new StdFilDocManager();
	}

	@Override
	public Document getDocument() {
		// TODO Auto-generated method stub
		return manager.getDocument();
	}

	@Override
	public File getFile() {
		// TODO Auto-generated method stub
		return manager.getFile();
	}

	@Override
	public State getState() {
		// TODO Auto-generated method stub
		return manager.getState();
	}

	@Override
	public boolean isSynchronized() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clearDoc() throws PetException {
		try {
			manager.getDocument().remove(0, manager.getDocument().getLength());
		} catch (BadLocationException e) {
			throw new PetException(e.getMessage());
		}
	}

	@Override
	public void removeDocAndFile() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resetCurrentDocWithCurrentFile() throws PetException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveCurrentDocIntoCurrentFile() throws PetException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveCurrentDocIntoFile(File f) throws PetException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNewDocAndNewFile(File f) throws PetException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNewDocFromFile(File f) throws PetException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setNewDocWithoutFile() {
		// TODO Auto-generated method stub
		
	}

}
