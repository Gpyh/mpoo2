package serie11.model;

public class PetException extends Exception {
    
    public PetException() {
        super();
    }
    
    public PetException(String msg) {
        super(msg);
    }
}
