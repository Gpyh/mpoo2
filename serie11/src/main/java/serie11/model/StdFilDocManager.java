package serie11.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import util.Contract;

class StdFilDocManager implements FilDocManager
{
	private final static int BUFFER_SIZE = 2048;

	private Document document;
	private File file;

	StdFilDocManager() {
		super();
	}

	@Override
	public Document getDocument() {
		return this.document;
	}

	@Override
	public File getFile() {
		return this.file;
	}

	@Override
	public State getState() {
		return State.values()[(file == null ? 0 : 2) + (document == null ? 0 : 1)];
	}

	@Override
	public void load() throws IOException, BadLocationException {
		Contract.checkCondition(getState() == State.FIL_DOC);
		document.remove(0, document.getLength());
		StringBuilder builder = new StringBuilder();

		Reader input = new InputStreamReader(new FileInputStream(file), "UTF-8");
		try {
			char[] buffer = new char[BUFFER_SIZE];
			int readlength = input.read(buffer);
			while(readlength > 0) {
				builder.append(buffer, 0, readlength);
				readlength = input.read(buffer);
			}
		} finally {
			input.close();
		}

		document.insertString(0, builder.toString(), null);
	}

	@Override
	public void removeDocument() {
		this.document = null;
	}

	@Override
	public void removeFile() {
		this.file = null;
	}

	@Override
	public void save() throws IOException, BadLocationException {
		Contract.checkCondition(getState() == State.FIL_DOC);
		Writer output = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
		try {
			output.write(document.getText(0, document.getLength()));
		} finally {
			output.close();
		}
	}

	@Override
	public void setDocument(Document d) {
		Contract.checkCondition(d != null);
		this.document = d;
	}

	@Override
	public void setFile(File f) {
		Contract.checkCondition(f != null);
		Contract.checkCondition(f.isFile());
		this.file = f;
	}

}
