package serie04;

import util.Contract;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by gpyh on 10/9/15.
 */
public enum Civ {
    UKN(""),
    MR("M."),
    MRS("Mme"),
    MS("Mlle");

    private final List<Civ> allowedChanges;
    private final String toString;

    private Civ(String toString){
        this.toString = toString;
        this.allowedChanges = new ArrayList<Civ>();
    }

    static {
        UKN.allowChanges(MR,MRS,MS);
        MRS.allowChanges(MS);
        MS.allowChanges(MRS);
    }

    private void allowChanges(Civ... allowedChanges){
        this.allowedChanges.addAll(Arrays.asList(allowedChanges));
    }

    /**
     * @pre
     *     candidate != null
     * @post
     *     this == UKN ==> result == { MR, MRS, MS }.contains(candidate)
     *     this == MR  ==> result == false
     *     this == MRS ==> result == (candidate == MS)
     *     this == MS  ==> result == (candidate == MRS)
     */
    public boolean canEvolveTo(Civ candidate){
        Contract.checkCondition(candidate != null);
        return this.allowedChanges.contains(candidate);
    };
    /**
     * @post
     *     this == UKN ==> result.equals("")
     *     this == MR  ==> result.equals("M.")
     *     this == MRS ==> result.equals("Mme.")
     *     this == MS  ==> result.equals("Mlle.")
     */
    public String toString(){
        return this.toString;
    }

}
