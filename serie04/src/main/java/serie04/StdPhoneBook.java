package serie04;

import util.Contract;

import java.util.*;

/**
 * Created by gpyh on 10/10/15.
 */
public class StdPhoneBook implements PhoneBook {

    private NavigableMap<Contact,List<String>> book;

    public StdPhoneBook(){
        this.book = new TreeMap<Contact, List<String>>();
    }

    @Override
    public String firstPhoneNumber(Contact p) {
        Contract.checkCondition(p != null);
        Contract.checkCondition(this.book.containsKey(p));
        List<String> nums = this.book.get(p);
        Contract.checkCondition(!nums.isEmpty());
        return nums.get(0);
    }

    @Override
    public List<String> phoneNumbers(Contact p) {
        Contract.checkCondition(p != null);
        Contract.checkCondition(this.book.containsKey(p));
        return Collections.unmodifiableList(this.book.get(p));
    }

    @Override
    public NavigableSet<Contact> contacts() {
        return book.navigableKeySet();
    }

    @Override
    public boolean contains(Contact p) {
        Contract.checkCondition(p != null);
        return this.book.containsKey(p);
    }

    @Override
    public boolean isEmpty() {
        return (this.book.size() == 0);
    }

    @Override
    public void addEntry(Contact p, List<String> nums) {
        Contract.checkCondition(p != null);
        Contract.checkCondition(!this.contains(p));
        Contract.checkCondition(nums != null);
        Contract.checkCondition(nums.size() > 0);
        Set<String> setnums = new LinkedHashSet<String>(nums);
        nums = new ArrayList<String>(setnums);
        this.book.put(p,nums);
    }

    @Override
    public void addPhoneNumber(Contact p, String n) {
        Contract.checkCondition(p != null);
        Contract.checkCondition(n != null);
        Contract.checkCondition(!n.equals(""));
        if(this.book.containsKey(p)){
            List<String> nums = this.book.get(p);
            if(!nums.contains(n)) {
                nums.add(n);
            }
        } else {
            List<String> nums = new ArrayList<String>();
            nums.add(n);
            this.book.put(p,nums);
        }
    }

    @Override
    public void removeEntry(Contact p) {
        Contract.checkCondition(p != null);
        Contract.checkCondition(this.book.containsKey(p));
        this.book.remove(p);
    }

    @Override
    public void deletePhoneNumber(Contact p, String n) {
        Contract.checkCondition(p != null);
        Contract.checkCondition(this.book.containsKey(p));
        Contract.checkCondition(n != null);
        Contract.checkCondition(!n.equals(""));
        List<String> nums = this.book.get(p);
        nums.remove(n);
        if(nums.isEmpty()) {
            this.book.remove(p);
        }
    }

    @Override
    public void clear() {
        this.book = new TreeMap<Contact, List<String>>();
    }
}
