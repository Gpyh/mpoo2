package serie04;

import util.Contract;
import serie04.CivTest;

import static java.util.Objects.hash;

public class StdContact implements Contact {

    private String lastname;
    private String firstname;
    private Civ civility;

    public StdContact(String lastname, String firstname) {
        Contract.checkCondition(lastname != null);
        Contract.checkCondition(firstname != null);
        Contract.checkCondition(!lastname.equals("") || !firstname.equals(""));
        this.lastname = lastname;
        this.firstname = firstname;
        this.civility = Civ.UKN;
    }

    public StdContact(Civ civility, String lastname, String firstname) {
        Contract.checkCondition(civility != null);
        Contract.checkCondition(lastname != null);
        Contract.checkCondition(firstname != null);
        Contract.checkCondition(!lastname.equals("") || !firstname.equals(""));
        this.lastname = lastname;
        this.firstname = firstname;
        this.civility = civility;
    }

    @Override
    public int compareTo(Contact c) {
        int compareLastNames = this.getLastName().compareTo(c.getLastName());
        if(compareLastNames != 0) {
            return compareLastNames;
        } else {
            int compareFirstNames = this.getFirstName().compareTo(c.getFirstName());
            if(compareFirstNames != 0){
                return compareFirstNames;
            } else {
                int compareCivility = this.civility.ordinal() - c.getCivility().ordinal();
                return compareCivility;
            }
        }
    }

    @Override
    public Civ getCivility() {
        return this.civility;
    }

    @Override
    public String getFirstName() {
        return this.firstname;
    }

    @Override
    public String getLastName() {
        return this.lastname;
    }

    @Override
    public void setCivility(Civ civility) {
        Contract.checkCondition(civility != null,
                "You must give a valid civility");
        Contract.checkCondition(this.civility.canEvolveTo(civility),
                "Cannot change civility \"" + this.civility +
                "\" to \"" + civility + "\".");
        this.civility = civility;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        } else if(this.getClass() != obj.getClass()) {
            return false;
        } else {
            Contact contact = (StdContact) obj;
            return (this.getFirstName() == contact.getFirstName() &&
                this.getLastName() == contact.getLastName() &&
                this.getCivility() == contact.getCivility());
        }
    }

    @Override
    public int hashCode() {
        return hash(this.getCivility().toString() + this.getFirstName() + this.getLastName());
    }

    @Override
    public String toString() {
        return this.civility + " " + this.firstname + " " + this.lastname;
    }
}
