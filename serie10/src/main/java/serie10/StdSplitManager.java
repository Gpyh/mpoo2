package serie10;

import util.Contract;

import java.io.*;
import java.util.Observable;

public class StdSplitManager extends Observable implements SplitManager {

    private static final int BUFFER_SIZE = 8192;

    private File f;
    private long[] splitsSizes;

    public StdSplitManager() {
        setFile(new File(""));
    }

    public StdSplitManager(File f) {
        setFile(f);
    }

    private int ceildiv(long a, long b) {
        return (int) ((a + b - 1) / b);
    }

    @Override
    public File getFile() {
        return f;
    }

    @Override
    public int getMaxFragmentNb() {
        int maxNb;
        if (!f.exists() || f.length() == 0) {
            return 1;
        }
        maxNb = ceildiv(f.length(), MIN_FRAGMENT_SIZE);
        if(maxNb > MAX_FRAGMENT_NB) {
            return MAX_FRAGMENT_NB;
        } else {
            return maxNb;
        }
    }

    @Override
    public String[] getSplitsNames() {
        String fName = getFile().getAbsolutePath();
        int number = splitsSizes.length;
        String[] splitsNames = new String[number];
        for (int i = 1; i <= number; i++) {
            splitsNames[i - 1] = fName + "." + i;
        }
        return splitsNames;
    }

    @Override
    public long[] getSplitsSizes() {
        return this.splitsSizes.clone();
    }

    @Override
    public void setFile(File f) {
        Contract.checkCondition(f != null);
        this.f = f;
        if(!f.exists() || f.length() == 0) {
            this.splitsSizes = new long[0];
        } else {
            this.splitsSizes = new long[1];
            this.splitsSizes[0] = f.length();
        }
    }

    @Override
    public void setSplitsSizes(long fragSize) {
        Contract.checkCondition(fragSize >= this.MIN_FRAGMENT_SIZE);
        long length = getFile().length();
        Contract.checkCondition(length > 0);
        int number = ceildiv(length, fragSize);
        splitsSizes = new long[number];
        for (int i = 0; i < number - 1; i++) {
            splitsSizes[i] = fragSize;
        }
        splitsSizes[number - 1] = length % fragSize;
    }

    @Override
    public void setSplitsSizes(long[] fragSizes) {
        Contract.checkCondition(fragSizes != null);
        Contract.checkCondition(fragSizes.length > 0);
        Contract.checkCondition(fragSizes.length <= getMaxFragmentNb());
        Contract.checkCondition(getFile().length() > 0);

        for (long fragSize : fragSizes) {
            Contract.checkCondition(fragSize >= this.MIN_FRAGMENT_SIZE);
        }

        long left = getFile().length();
        int i = 0;
        while (left > 0 && i < fragSizes.length) {
            left -= fragSizes[i++];
        }

        int number;
        if (left <= 0) {
            number = i;
            this.splitsSizes = new long[number];
            for (int j = 0; j < number - 1; j++) {
                splitsSizes[j] = fragSizes[j];
            }
            splitsSizes[number - 1] = left + fragSizes[number - 1];
        } else {
            number = fragSizes.length + 1;
            this.splitsSizes = new long[number];
            for (int j = 0; j < fragSizes.length; j++) {
                splitsSizes[j] = fragSizes[j];
            }
            splitsSizes[number - 1] = left;
        }
    }

    @Override
    public void setSplitsNumber(int number) {
        Contract.checkCondition(number > 0);
        Contract.checkCondition(number <= getMaxFragmentNb());
        long length = getFile().length();
        Contract.checkCondition(length > 0);

        long fragSize;
        fragSize = length / number;
        if (fragSize < MIN_FRAGMENT_SIZE) {
            number = ceildiv(length, MIN_FRAGMENT_SIZE);
            this.splitsSizes = new long[number];
            for (int i = 0; i < number - 1; i++) {
                splitsSizes[i] = MIN_FRAGMENT_SIZE;
            }
            splitsSizes[number - 1] = length - MIN_FRAGMENT_SIZE*(number - 1);
        } else {
            this.splitsSizes = new long[number];
            int i = 0;
            for (; i < length % number; i++) {
                splitsSizes[i] = fragSize + 1;
            }
            for (; i < number; i++) {
                splitsSizes[i] = fragSize;
            }
        }
    }

    @Override
    public void split() throws IOException {
        Contract.checkCondition(getFile().exists());

        String[] splitsNames = getSplitsNames();

        FileInputStream in = null;
        FileOutputStream out = null;

        try {
            in = new FileInputStream(getFile());
            byte[] buffer = new byte[BUFFER_SIZE];
            int bread;
            long left;

            for (int i = 0; i < splitsSizes.length; i++) {
                left = getSplitsSizes()[i];
                out = new FileOutputStream(splitsNames[i]);
                while ((bread = in.read(buffer, 0, (int) Math.min(left, BUFFER_SIZE))) != -1 && left > 0) {
                    out.write(buffer, 0, bread);
                    left -= bread;
                }
                out.close();
            }
        } finally {
            if (in != null) {
                in.close();
            }
            if (out != null) {
                out.close();
            }
        }

    }
}

