package serie10;

import javax.swing.*;
import java.awt.*;

/**
 * Created by gpyh on 12/1/15.
 */
public class Splitter {

    private static final String FILE_LABEL = "Fichier à fragmenter : ";
    private static final String NUMBER_FRAG_LABEL = "Nb. fragments :";
    private static final String SIZE_FRAG_LABEL = "Taille des fragments* :";
    private static final String BYTES_LABEL = " octets";
    private static final String INFO_BAR = "(*) Il s'agit de la taille de chaque fragment à un octet près, " +
            "sauf peut-être pour le dernier fragment.";
    private static final String BROWSE_LABEL = "Parcourir...";
    private static final String FRAG_LABEL = "Fragmenter !";

    private SplitManager model;

    private JFrame mainFrame;

    private JTextField splitFileName;
    private JButton browseButton;
    private JTextArea splitDescription;
    private JComboBox<Integer> splitFragmentsNb;
    private JTextField splitFragmentSize;
    private JButton splitButton;

    public Splitter() {
        createModel();
        createView();
        placeComponent();
        createController();
    }

    public void display() {
        refresh();
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    private void createModel() {
        this.model = new StdSplitManager();
    }

    private void createView() {
        this.mainFrame = new JFrame();
        this.splitFileName = new JTextField("                  ");
        this.browseButton = new JButton(BROWSE_LABEL);
        this.splitDescription = new JTextArea("Doing \n shitty things \n all the way");
        Integer[] integers = new Integer[0];
        this.splitFragmentsNb = new JComboBox<Integer>(integers);
        this.splitFragmentSize = new JTextField("            ");
        this.splitButton = new JButton(FRAG_LABEL);
    }

    private void placeComponent() {
        JPanel p = new JPanel(); {
            p.add(new JLabel(FILE_LABEL));
            p.add(this.splitFileName);
            p.add(this.browseButton);
        }
        mainFrame.add(p, BorderLayout.NORTH);
        p = new JPanel(new GridLayout(2, 1)); {
            JPanel q = new JPanel(new BorderLayout()); {
                JPanel r = new JPanel(new GridLayout(2, 2)); {
                    JPanel s = new JPanel(new FlowLayout(FlowLayout.RIGHT));
                    {
                        s.add(new JLabel(NUMBER_FRAG_LABEL));
                    }
                    r.add(s);
                    s = new JPanel(new FlowLayout(FlowLayout.LEFT));
                    {
                        s.add(this.splitFragmentsNb);
                    }
                    r.add(s);
                    s = new JPanel(new FlowLayout(FlowLayout.RIGHT));
                    {
                        s.add(new JLabel(SIZE_FRAG_LABEL));
                    }
                    r.add(s);
                    s = new JPanel(new FlowLayout(FlowLayout.LEFT));
                    {
                        s.add(this.splitFragmentSize);
                        s.add(new JLabel(BYTES_LABEL));
                    }
                    r.add(s);
                }
                q.add(r, BorderLayout.SOUTH);
            }
            p.add(q);
            q = new JPanel(new BorderLayout()); {
                JPanel r = new JPanel(); {
                    r.add(this.splitButton);
                }
                q.add(r, BorderLayout.NORTH);
            }
            p.add(q);
        }
        mainFrame.add(p, BorderLayout.WEST);
        JScrollPane scrollPane = new JScrollPane(); {
            scrollPane.add(splitDescription);
        }
        mainFrame.add(scrollPane, BorderLayout.CENTER);
        mainFrame.add(new JLabel(INFO_BAR), BorderLayout.SOUTH);
    }

    private void createController() {
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void refresh() {

    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Splitter().display();
            }
        });
    }

}
