package serie01.model;

import serie01.util.Currency;
import serie01.util.CurrencyId;
import util.Contract;

public class StdMultiConverter implements MultiConverter {

    private final Currency[] currencies;
    private double amountEur;

    public StdMultiConverter(int n){
        Contract.checkCondition(n >= 2);

        this.currencies = new Currency[n];
        this.amountEur = 0;

        Currency euro = Currency.get(CurrencyId.EUR);
        for (int i = 0; i < n; i++) {
            this.currencies[i] = euro;
        }
    }

    // Factorisation pas fondamentalement utile
    private void checkIndex(int index){
        Contract.checkCondition(index >= 0);
        Contract.checkCondition(index < getCurrencyNb());
    }

    // On préfère appeler getCurrency dans les autres méthodes car on y effectue déjà le test
    // de nullité sur l'index
    @Override
    public Currency getCurrency(int index) {
        checkIndex(index);
        return currencies[index];
    }


    // Convertir depuis un montant en euros vers le montant dans la devise d'index renseigné
    private double fromEur(int index, double amountEur) {
        Contract.checkCondition(amountEur >= 0.0);
        return amountEur*getCurrency(index).getExchangeRate();
    }

    // Convertir depuis un montant dans la devise d'index renseignée vers le montant en euros
    private double toEur(int index, double amount) {
        Contract.checkCondition(amount >= 0.0);
        return amount/getCurrency(index).getExchangeRate();
    }

    @Override
    public double getAmount(int index) {
        return fromEur(index, this.amountEur);
    }

    @Override
    public int getCurrencyNb() {
        return currencies.length;
    }

    @Override
    public double getExchangeRate(int index1, int index2) {
        return getCurrency(index2).getExchangeRate()/getCurrency(index1).getExchangeRate();
    }

    @Override
    public void setAmount(int index, double amount) {
        this.amountEur = toEur(index, amount);
    }

    @Override
    public void setCurrency(int index, Currency c) {
        checkIndex(index);
        Contract.checkCondition(c != null);
        currencies[index] = c;
    }
}
