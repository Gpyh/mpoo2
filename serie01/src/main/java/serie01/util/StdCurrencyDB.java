package serie01.util;

import util.Contract;

public class StdCurrencyDB implements CurrencyDB {

    // Il m'a été rapporté qu'en TD, vous avez indiqué préféré
    // le moins d'accès possible aux informations de CurrencyId durant la vie de l'objet,
    // c'est pourquoi l'ensemble des informations ont été recopiées dans les tableaux respectifs
    // D'autres façons de faire :
    // - Appeler directement les méthodes afférentes dans CurrencyId
    // - Créer une matrice de taille n*3, n étant le nombre de CurrencyId, pour stocker les informations
    //      plutôt que d'utiliser des tables isolées
    // - Utiliser une classe interne mimant CurrencyId et renseigner comme attribut un tableau d'objets
    //      de cette classe

	private final String[] isoCodes;
	private final String[] names;
	private final String[] lands;
	private final double[] rates;

	public StdCurrencyDB() {

		// La méthode values() copie le tableau de la superclasse Enum.
		// Il est donc préférable de l'appeler une unique fois.
		CurrencyId[] currencies = CurrencyId.values();
		
		isoCodes = new String[currencies.length];
		names = new String[currencies.length];
		lands = new String[currencies.length];
		rates = new double[currencies.length];
		
		CurrencyId c;
		
		for (int i = 0; i < rates.length; i++) {
			c = currencies[i];
			isoCodes[i] = c.getIsoCode();
			names[i] = c.getName();
			lands[i] = c.getLand();
			rates[i] = c.getRateForYear2001();
		}
	}

    // Factorisation pas fondamentalement utile
	private void checkNotNull(Object obj) {
		Contract.checkCondition(obj != null);
	}

	@Override
	public double getExchangeRate(CurrencyId id) {
		checkNotNull(id);
		return rates[id.ordinal()];
	}

	@Override
	public String getIsoCode(CurrencyId id) {
		checkNotNull(id);
		return isoCodes[id.ordinal()];
	}

	@Override
	public String getLand(CurrencyId id) {
		checkNotNull(id);
		return lands[id.ordinal()];
	}

	@Override
	public String getName(CurrencyId id) {
		checkNotNull(id);
		return names[id.ordinal()];
	}

	@Override
	public void setExchangeRate(CurrencyId id, double rate) {
		checkNotNull(id);
		Contract.checkCondition(rate > 0);
		rates[id.ordinal()]=rate;
	}
}
