package serie08;

import serie06.*;
import util.Contract;

import java.util.EnumMap;
import java.util.Observable;

public class StdDrinksMachineModel extends Observable implements DrinksMachineModel {

    private Stock<DrinkTypes> drinks;
    private DrinkTypes lastDrink;
    private MoneyAmount cash;
    private MoneyAmount credit;
    private MoneyAmount change;

    public StdDrinksMachineModel() {
        this.drinks = new StdStock<DrinkTypes>(new EnumMap<DrinkTypes, Integer>(DrinkTypes.class));
        this.cash = new StdMoneyAmount();
        this.credit = new StdMoneyAmount();
        this.change = new StdMoneyAmount();
        this.lastDrink = null;
    }

    @Override
    public int getDrinkNb(DrinkTypes d) {
        Contract.checkCondition(d != null);
        return drinks.getNumber(d);
    }

    @Override
    public DrinkTypes getLastDrink() {
        return this.lastDrink;
    }

    @Override
    public int getCreditAmount() {
        return credit.getTotalValue();
    }

    @Override
    public int getCreditNb(CoinTypes c) {
        Contract.checkCondition(c != null);
        return credit.getNumber(c);
    }

    @Override
    public int getCashAmount() {
        return cash.getTotalValue();
    }

    @Override
    public int getCashNb(CoinTypes c) {
        Contract.checkCondition(c != null);
        return cash.getNumber(c);
    }

    @Override
    public int getChangeAmount() {
        return change.getTotalValue();
    }

    @Override
    public int getChangeNb(CoinTypes c) {
        Contract.checkCondition(c != null);
        return change.getNumber(c);
    }


    @Override
    public boolean canGetChange() {
        return (credit.computeChange(199) != null);
    }

    @Override
    public void selectDrink(DrinkTypes d) {
        Contract.checkCondition(d != null);
        Contract.checkCondition(getDrinkNb(d) >= 1);
        Contract.checkCondition(getCreditAmount() >= d.getPrice());
        Contract.checkCondition(getLastDrink() == null);

        drinks.removeElement(d);
        this.lastDrink = d;

        boolean canGiveChange = canGetChange();
        this.cash.addAmount(this.credit);
        if(canGiveChange) {
            MoneyAmount giveBack = this.cash.computeChange(this.credit.getTotalValue() - d.getPrice());
            this.change.addAmount(giveBack);
            this.cash.removeAmount(giveBack);
        }
        this.credit.reset();
        setChanged();
        notifyObservers();
    }

    @Override
    public void fillStock(DrinkTypes d, int q) {
        Contract.checkCondition(d != null);
        Contract.checkCondition(q > 0 && q + getDrinkNb(d) <= MAX_DRINK);
        drinks.addElement(d,q);
    }

    @Override
    public void fillCash(CoinTypes c, int q) {
        Contract.checkCondition(c != null);
        Contract.checkCondition(q > 0 && q + getCashNb(c) <= MAX_COIN);
        cash.addElement(c,q);
    }

    @Override
    public void insertCoin(CoinTypes c) {
        Contract.checkCondition(c != null);
        credit.addElement(c);
        setChanged();
        notifyObservers();
    }

    @Override
    public void cancelCredit() {
        this.change.addAmount(this.credit);
        this.credit.reset();
    }

    @Override
    public void takeDrink() {
        this.lastDrink = null;
        System.out.println("Boisson rendue");
        setChanged();
        notifyObservers();
    }

    @Override
    public void takeChange() {
        change.reset();
        System.out.println("Monnaie rendue");
        setChanged();
        notifyObservers();
    }

    @Override
    public void reset() {
        this.lastDrink = null;
        this.drinks.reset();
        this.cash.reset();
        this.change.reset();
        this.credit.reset();
    }
}
