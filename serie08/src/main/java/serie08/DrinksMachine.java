package serie08;

import serie06.CoinTypes;
import serie06.DrinkTypes;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EnumMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class DrinksMachine {

    private static final int MAX_DIGITS_CURRENCY = 3;
    private static final int MAX_CHARACTERS_DRINK = 15;
    private static final String APP_TITLE = "Distributeur de boissons";
    private static final String INSERT_LABEL = "Insérer";
    private static final String CANCEL_LABEL = "Annuler";
    private static final String RECEIVE_LABEL = "Prenez votre boisson et/ou votre monnaie";
    private static final String CURRENCY_LABEL = "cents";
    private static final String CHANGE_LABEL = "Monnaie : ";
    private static final String DRINK_LABEL = "Boisson : ";
    private static final String CANGETCHANGE_LABEL = "Cet appareil rend la monnaie";
    private static final String CANNOTGETCHANGE_LABEL = "Cet appareil ne rend pas la monnaie";
    private static final String CREDIT_LABEL = "Vous disposez d'un crédit de ";
    private static final String NOT_ENOUGH_CREDIT_TITLE = "Crédit insuffisant";
    private static final String NOT_ENOUGH_CREDIT_MESSAGE =
            "Vous ne disposez pas d'assez de crédit.\n" +
            "Veuillez insérer des pièces.";
    private static final String INVALID_COIN_TITLE = "Pièce invalide";
    private static final String INVALID_COIN_MESSAGE =
        "Veuillez insérer une pièce valide\n";

    private DrinksMachineModel model;

    private JFrame mainFrame;
    private Map<DrinkTypes, JButton> drinkButtons;
    private JButton insertButton;
    private JButton cancelButton;
    private JButton receiveButton;
    private JTextField insertField;

    private JTextField drinkView;
    private JTextField changeView;
    private JLabel canGetChangeView;
    private JLabel creditView;

    private class DrinkActionListener implements ActionListener {

        private DrinkTypes drinkType;
        DrinkActionListener(DrinkTypes drinkType) {
            this.drinkType = drinkType;
        }
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("YUP" + drinkType.toString());
            if(model.getCreditAmount() >= this.drinkType.getPrice() && model.getDrinkNb(this.drinkType) >= 1) {
                model.selectDrink(this.drinkType);
            } else {
                JOptionPane.showMessageDialog(
                        mainFrame,
                        NOT_ENOUGH_CREDIT_MESSAGE,
                        NOT_ENOUGH_CREDIT_TITLE,
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public DrinksMachine() {
        createModel();
        createView();
        placeComponents();
        createController();
    }

    public void display() {
        refresh();
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }

    private void createModel() {
        this.model = new StdDrinksMachineModel();
        for(DrinkTypes dt : DrinkTypes.values()) {
            model.fillStock(dt, 2);
        }
    }


    private void createView() {
        final int frameWidth = 600;
        final int frameHeight = 430;

        this.mainFrame = new JFrame(APP_TITLE);
        this.mainFrame.setPreferredSize(new Dimension(frameWidth, frameHeight));

        this.drinkButtons = new EnumMap<DrinkTypes, JButton>(DrinkTypes.class);
        for(DrinkTypes dt : DrinkTypes.values()) {
            this.drinkButtons.put(dt, new JButton(dt.toString()));
        }
        this.insertButton = new JButton(INSERT_LABEL);
        this.cancelButton = new JButton(CANCEL_LABEL);
        this.receiveButton = new JButton(RECEIVE_LABEL);
        this.insertField = new JTextField(MAX_DIGITS_CURRENCY);

        this.drinkView = new JTextField(MAX_CHARACTERS_DRINK); {
            this.drinkView.setEditable(false);
            this.drinkView.setFocusable(false);
        }
        this.changeView = new JTextField(MAX_DIGITS_CURRENCY); {
            this.changeView.setEditable(false);
            this.changeView.setFocusable(false);
        }
        this.canGetChangeView = new JLabel(CANGETCHANGE_LABEL);
        this.creditView = new JLabel(CREDIT_LABEL + "0 " + CURRENCY_LABEL);

    }

    private void createController() {
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        model.addObserver(new Observer() {
            public void update(Observable o, Object arg) {
                refresh();
            }
        });

        for(DrinkTypes dt : DrinkTypes.values()) {
            System.out.println(dt.toString());
            drinkButtons.get(dt).addActionListener(new DrinkActionListener(dt));
        }

        this.insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    CoinTypes coin = CoinTypes.getCoinType(Integer.parseInt(insertField.getText()));
                    if(coin == null) {
                        errorDialog();
                    } else {
                        insertField.setText("");
                        model.insertCoin(coin);
                    }
                } catch(NumberFormatException exception) {
                    errorDialog();
                }
            }
            private void errorDialog() {
                  JOptionPane.showMessageDialog(
                      mainFrame,
                      INVALID_COIN_MESSAGE,
                      INVALID_COIN_TITLE,
                      JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private void placeComponents() {
        JPanel p = new JPanel(new GridLayout(0,1)); {
            JPanel q = new JPanel(); {
                q.add(this.canGetChangeView);
            }
            p.add(q);
            q = new JPanel(); {
                q.add(this.creditView);
            }
            p.add(q);
        }
        this.mainFrame.add(p, BorderLayout.NORTH);
        p = new JPanel(new GridLayout(0,2)); {
            for(DrinkTypes dt : DrinkTypes.values()) {
                p.add(this.drinkButtons.get(dt));
                p.add(new JLabel(dt.getPrice() + " " + CURRENCY_LABEL));
            }
        }
        this.mainFrame.add(p, BorderLayout.WEST);
        p = new JPanel(new FlowLayout(FlowLayout.CENTER)); {
            JPanel q = new JPanel(new GridLayout(2,2)); {
                q.add(this.insertButton);
                JPanel r = new JPanel(); {
                    r.add(this.insertField);
                    r.add(new JLabel(" " + CURRENCY_LABEL));
                }
                q.add(r);
                q.add(this.cancelButton);
            }
            p.add(q);
        }
        this.mainFrame.add(p, BorderLayout.EAST);
        p = new JPanel(new GridLayout(0,1)); {
            JPanel q = new JPanel(new GridLayout(1,0)); {
                JPanel r = new JPanel(); {
                    r.add(new JLabel(DRINK_LABEL));
                    r.add(this.drinkView);
                }
                q.add(r);
                r = new JPanel(); {
                    r.add(new JLabel(CHANGE_LABEL));
                    r.add(this.changeView);
                    r.add(new JLabel(" " + CURRENCY_LABEL));
                }
                q.add(r);
            }
            p.add(q);
            q = new JPanel(); {
                q.add(this.receiveButton);
            }
            p.add(q);
        }
        this.mainFrame.add(p, BorderLayout.SOUTH);
    }


    private void refresh() {
        this.canGetChangeView.setText(model.canGetChange() ? CANGETCHANGE_LABEL: CANNOTGETCHANGE_LABEL);
        this.creditView.setText(CREDIT_LABEL + model.getCreditAmount() + " " + CURRENCY_LABEL);
        this.drinkView.setText(model.getLastDrink() == null ? "" : model.getLastDrink().toString());
        System.out.println(model.getChangeAmount());
        this.changeView.setText(model.getChangeAmount() == 0 ? "" : "" + model.getChangeAmount());
        for(DrinkTypes dt : DrinkTypes.values()) {
            this.drinkButtons.get(dt).setEnabled(model.getDrinkNb(dt) >= 1);
        }
    }

    public static void main(String[] argv) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new DrinksMachine().display();
            }
        });
    }
}
