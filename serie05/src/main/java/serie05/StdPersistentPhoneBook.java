package serie05;

import serie04.Civ;
import serie04.Contact;
import serie04.StdContact;
import serie04.StdPhoneBook;
import util.Contract;

import java.io.*;
import java.util.*;

public class StdPersistentPhoneBook extends StdPhoneBook implements PersistentPhoneBook {

    private File file;

    public StdPersistentPhoneBook(){
        super();
        this.file = null;
    }
    public StdPersistentPhoneBook(File file){
        super();
        this.file = file;
    }

    @Override
    public File getFile() {
        return this.file;
    }

    @Override
    public void setFile(File file) {
        Contract.checkCondition(file != null);
        this.file = file;
    }

    @Override
    public void load() throws IOException, BadSyntaxException {
        this.clear();
        Contract.checkCondition(getFile() != null);
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(getFile()));
            String line;
            String[] entry;
            List<String[]> entries = new ArrayList<String[]>();
            while((line = bufferedReader.readLine()) != null) {
                if(this.LINE_RECOGNIZER.matcher(line).find()) {
                    entry = line.split(":");
                    for(int i = 0; i < entry.length; i++) {
                        entry[i] = entry[i].trim();
                    }
                    entries.add(entry);
                } else {
                    throw new BadSyntaxException();
                }
            }
            bufferedReader.close();
            String[] nums;
            for(String[] e : entries) {
                nums = e[3].split(",");
                for(int i = 0; i < nums.length; i++) {
                    nums[i] = nums[i].trim();
                }
                addEntry(new StdContact(
                                Civ.values()[Integer.parseInt(e[0])], e[1], e[2]),
                        Arrays.asList(nums));
            }
        } catch (IOException e) {
            this.clear();
            throw e;
        }
    }

    private void saveEntry(Contact p, StringBuilder builder) throws IOException {
        Iterator<String> phoneNumbersIterator = phoneNumbers(p).iterator();
        builder.append(p.getCivility().ordinal() + ":");
        builder.append(p.getLastName() + ":");
        builder.append(p.getFirstName() + ":");
        builder.append(phoneNumbersIterator.next());
        while(phoneNumbersIterator.hasNext()) {
            builder.append("," + phoneNumbersIterator.next());
        }
    }

    @Override
    public void save() throws IOException {
        Contract.checkCondition(getFile() != null);
        try {
            Writer writer = new FileWriter(getFile());
            StringBuilder builder = new StringBuilder();
            Iterator<Contact> contactsIterator = contacts().iterator();
            if(contactsIterator.hasNext()) {
                saveEntry(contactsIterator.next(), builder);
            }
            while(contactsIterator.hasNext()) {
                builder.append('\n');
                saveEntry(contactsIterator.next(),builder);
            }
            writer.write(builder.toString());
            writer.close();
        } catch (IOException e) {
            throw e;
        }
    }
}
