package serie06;

import util.Contract;

import java.util.*;

/**
 * Created by gpyh on 10/18/15.
 */
public class StdStock<E> implements Stock<E> {

    private final Map<E,Integer> numbers;
    private int totalNumber;

    public StdStock() {
        this(new HashMap<E, Integer>());
    }

    public StdStock(Map<E, Integer> numbers) {
        Contract.checkCondition(numbers.isEmpty());
        this.totalNumber = 0;
        this.numbers = numbers;
    }

    @Override
    public int getNumber(E e) {
        Contract.checkCondition(e != null);
        if(numbers.containsKey(e)) {
            return numbers.get(e);
        } else {
            return 0;
        }
    }

    @Override
    public int getTotalNumber() {
        return this.totalNumber;
    }

    @Override
    public void addElement(E e) {
        addElement(e,1);
    }

    @Override
    public void addElement(E e, int qty) {
        Contract.checkCondition(e != null);
        Contract.checkCondition(qty > 0);
        int number = 0;
        if(numbers.containsKey(e)) {
            number = numbers.get(e);
        }
        numbers.put(e,number+qty);
        increaseTotal(qty);
    }

    @Override
    public void removeElement(E e) {
        removeElement(e,1);
    }

    @Override
    public void removeElement(E e, int qty) {
        Contract.checkCondition(e != null);
        Contract.checkCondition(qty > 0);
        int number = getNumber(e);
        Contract.checkCondition(number >= qty);
        numbers.put(e,number-qty);
        decreaseTotal(qty);
    }

    @Override
    public void reset() {
        this.totalNumber = 0;
        this.numbers.clear();
    }

    private void increaseTotal(int qty) {
        this.totalNumber = this.totalNumber + qty;
    }

    private void decreaseTotal(int qty) {
        this.totalNumber = this.totalNumber - qty;
    }
}
