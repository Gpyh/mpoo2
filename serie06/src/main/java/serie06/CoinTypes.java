package serie06;

/**
 * Created by gpyh on 10/18/15.
 */
public enum CoinTypes {
    ONE(1) {
        @Override
        public String toString() {
            return getFaceValue() + " ct";
        }
    },
    TWO(2),
    FIVE(5),
    TEN(10),
    TWENTY(20),
    FIFTY(50),
    ONE_HUNDRED(100),
    TWO_HUNDRED(200);

    private final int faceValue;

    private CoinTypes(int faceValue) {
        this.faceValue = faceValue;
    }

    public int getFaceValue() {
        return this.faceValue;
    }

    @Override
    public String toString() {
        return getFaceValue() + " cts";
    }

    public static CoinTypes getCoinType(int faceValue) {
        for(CoinTypes ct : CoinTypes.values()) {
            if(ct.getFaceValue() == faceValue) {
                return ct;
            }
        }
        return null;
    }
}
