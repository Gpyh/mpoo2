package serie06;

/**
 * Created by gpyh on 10/18/15.
 */
public enum DrinkTypes {
    COFFEE(30,"coffee"),
    CHOCOLATE(45,"chocolate"),
    ORANGE_JUICE(110,"orange juice");

    private final int price;
    private final String toString;

    private DrinkTypes(int price, String toString) {
        this.price = price;
        this.toString = toString;
    }

    public int getPrice() {
        return this.price;
    }

    @Override
    public String toString() {
        return this.toString;
    }
}
