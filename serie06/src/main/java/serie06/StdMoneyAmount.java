package serie06;

import util.Contract;

import java.util.EnumMap;

public class StdMoneyAmount extends StdStock<CoinTypes> implements MoneyAmount {

    private int totalValue;

    private void increaseTotalValue(int v) {
        this.totalValue = this.totalValue + v;
    }

    private void decreaseTotalValue(int v) {
        this.totalValue = this.totalValue - v;
    }

    public StdMoneyAmount() {
        super(new EnumMap<CoinTypes, Integer>(CoinTypes.class));
        this.totalValue = 0;
    }

    @Override
    public void reset() {
        super.reset();
        this.totalValue = 0;
    }

    @Override
    public void addElement(CoinTypes c, int qty) {
        super.addElement(c, qty);
        increaseTotalValue(qty*c.getFaceValue());
    }

    @Override
    public void removeElement(CoinTypes c, int qty) {
        super.removeElement(c, qty);
        decreaseTotalValue(qty*c.getFaceValue());
    }

    @Override
    public int getValue(CoinTypes c) {
        Contract.checkCondition(c != null);
        return getNumber(c)*c.getFaceValue();
    }

    @Override
    public int getTotalValue() {
        return this.totalValue;
    }

    @Override
    public void addAmount(MoneyAmount amount) {
        Contract.checkCondition(amount != null);
        int number;
        for(CoinTypes c : CoinTypes.values()) {
            number = amount.getNumber(c);
            if(number > 0) {
                addElement(c, number);
            }
        }
    }

    @Override
    public MoneyAmount computeChange(int s) {
        Contract.checkCondition(s > 0);
        System.out.println("test");
        MoneyAmount change = new StdMoneyAmount();
        CoinTypes[] coinTypes = CoinTypes.values();
        CoinTypes c;
        int indexBiggestCoin = -1;
        for(int i = coinTypes.length - 1; i >= 0; i--) {
            if(this.getNumber(coinTypes[i]) > 0) {
               indexBiggestCoin = i;
            }
        }
        int toGiveBack;
        int number;
        int maxNumber;
        toGiveBack = s;
        change.reset();
        System.out.println(toGiveBack);
        for(int i = indexBiggestCoin; i >= 0; i--) {
            c = coinTypes[i];
            number = toGiveBack / c.getFaceValue();
            maxNumber = this.getNumber(c);
            if (number > maxNumber) {
                number = maxNumber;
            }
            if (number > 0) {
                change.addElement(c, number);
            }
            toGiveBack = toGiveBack - number * c.getFaceValue();
            System.out.println("Coins " + c.getFaceValue() + " in purse = " + this.getNumber(c));
            System.out.println("Coins " + c.getFaceValue() + " to take = " + number);
            System.out.println("TGB = " + toGiveBack);
        }
        if(toGiveBack > 0) {
            return null;
        } else {
            return change;
        }
    }

    @Override
    public void removeAmount(MoneyAmount amount) {
        Contract.checkCondition(amount != null);
        for (CoinTypes c : CoinTypes.values()) {
            Contract.checkCondition(this.getNumber(c) >= amount.getNumber(c));
        }
        int number;
        for (CoinTypes c : CoinTypes.values()) {
            number = amount.getNumber(c);
            if(number > 0) {
                removeElement(c, number);
            }
        }
    }
}
