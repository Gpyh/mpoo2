package serie03;

import serie03.cmd.Clear;
import serie03.cmd.Command;
import serie03.cmd.DeleteLine;
import serie03.cmd.InsertLine;
import util.Contract;

public class StdEditor implements Editor {
	
    private Text text;
    private History<Command> history;
    
    public StdEditor(){
    	this.text = new StdText();
    	this.history = new StdHistory<Command>(this.DEFAULT_HISTORY_SIZE);
    }

    public StdEditor(int historySize) {
    	Contract.checkCondition(historySize>0);
        this.text = new StdText();
        this.history = new StdHistory<Command>(historySize);
    }

    private void eatCommand(Command cmd){
        this.history.add(cmd);
        cmd.act();
    }
    @Override
    public int getTextLinesNb() {
        return this.text.getLinesNb();
    }

    @Override
    public String getTextContent() {
    	return this.text.getContent();
    }

    @Override
    public int getHistorySize() {
        return this.history.getMaxHeight();
    }

    @Override
    public int nbOfPossibleUndo() {
        return this.history.getCurrentPosition();
    }

    @Override
    public int nbOfPossibleRedo() {
        return this.history.getEndPosition()-this.history.getCurrentPosition();
    }

    @Override
    public void clear() {
        Command clear = new Clear(this.text);
        clear.act();
        this.history.add(clear);
    }

    @Override
    public void insertLine(int numLine, String s) {
    	Contract.checkCondition(numLine >=1);
    	Contract.checkCondition(numLine <= getTextLinesNb());
        eatCommand(new InsertLine(this.text,numLine,s));
    }

    @Override
    public void deleteLine(int numLine) {
        eatCommand(new DeleteLine(this.text,numLine));
    }

    @Override
    public void redo() {
        this.history.goForward();
        this.history.getCurrentElement().act();
    }

    @Override
    public void undo() {
    	Contract.checkCondition(nbOfPossibleUndo()>0);
        this.history.getCurrentElement().act();
        this.history.goBackward();
    }
}
