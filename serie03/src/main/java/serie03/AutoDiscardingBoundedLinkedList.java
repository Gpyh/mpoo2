package serie03;

import util.Contract;

public class AutoDiscardingBoundedLinkedList<T> {

    private final int capacity;
    private int size;
    private int cursor;

    private Entry<T> current;
    private Entry<T> first;

    /**
     * An auto-discarding bounded linked list.
     * It's a stack where element are inserted at the queue.
     * When it overflows, the head is lost.
     * You can move the position of the queue across the stack, back and forth.
     * Inserting an element at the queue discards any other element above.
     * It is implemented as LinkedList (elements are linked to those that come right before and after)
     * @inv
     *     0 <= getSize <= getCapacity
     *     0 <= getCurrentPosition <= getSize
     * @cons
     *     $ARGS$ int capacity
     *     $PRE$
     *         capacity > 0
     *     $POST$
     *         getCapacity == capacity
     *         getSize == 0
     *         getCurrentPosition == 0
     *         getCurrentElement == null
     */

    private static final class Entry<T> {
        T data;
        Entry<T> previous;
        Entry<T> next;
        Entry(T data) {
            System.out.println("Creating: " + data);
            this.data = data;
            this.previous = null;
            this.next = null;
        }
    }

    public AutoDiscardingBoundedLinkedList(int capacity){
        Contract.checkCondition(capacity>0);
        //System.out.println("Instantiate: capacity=" + capacity);
        this.capacity = capacity;
        this.size = 0;
        this.cursor = 0;
        this.current = null;
        this.first = null;
    }

    /**
     * @return
     *      The element in the current position
     */
    public T getCurrentElement(){
        System.out.println(this);
        return this.current.data;
    }

    /**
     * @return
     *      The current position
     */
    public int getCurrentPosition(){
        System.out.println(this);
        return this.cursor;
    }

    /**
     * @return
     *      The maximum number of elements
     */
    public int getCapacity(){
        System.out.println(this);
        return this.capacity;
    }

    /**
     * @return
     *      The current number of elements
     */
    public int getSize(){
        System.out.println(this);
        return this.size;
    }

    @Override
    public String toString() {
        return "Cap: " + capacity +", Size: "+size+", CP: "+cursor;
    }

    /**
     * Adds an element at the current position.
     * Discards the last elements if the size exceeds the capacity.
     * Discards the elements that come after the current position, if there is.
     * @param data
     *      The element to add
     * @post <pre>
     *     getCurrentElement() == data
     *     getCurrentPosition() <= getCapacity() ==>
     *         getSize() == old getSize() + 1
     *         getCurrentPosition() == old getCurrentPosition() + 1
     * </pre>
     */
    public void add(T data){
        System.out.println("add adbll");
        Entry<T> entry = new Entry(data);
        if(cursor == 0) {
            // Adding an element at the beginning gives a stack of size 1
            this.current = entry;
            this.first = entry;
            cursor = 1;
            size = 1;
        } else {
            // You add an element at the queue by putting the current element right before the added one
            this.current.next = entry;
            entry.previous = this.current;
            this.current = entry;
            if (cursor == capacity) {
                // When overflowing, delete the head
                this.first = this.first.next;
                this.first.previous = null;
            } else {
                // When not, move forward and increase in size
                cursor++;
                size = cursor;
            }
        }
    }

    /**
     * Move the queue forward ; only possible if you went backward before
     * @pre <pre>
     *      getCurrentPosition() < getSize()
     *  </pre>
     */
    public void stepForward(){
        Contract.checkCondition(cursor < size);
        cursor++;
        this.current=this.current.next;
    }

    /**
     * Move the queue backward ; only possible if you have already and element
     * @pre <pre>
     *      getCurrentPosition() > 0
     * </pre>
     */
    public void stepBackward(){
        Contract.checkCondition(cursor > 0);
        cursor--;
        this.current=this.current.previous;
    }
}
