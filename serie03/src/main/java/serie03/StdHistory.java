package serie03;

import util.Contract;

public class StdHistory<E> implements History<E> {

    /**
     * Implémenté comme une LinkedList FIFO avec curseur,
     * taille limitée et écrasement après insertion.
     */
    private final int maxHeight;
    private int endPosition;
    private int currentPosition;

    private Entry<E> currentEntry;
    private Entry<E> firstEntry;

    private static final class Entry<T> {
        T element;
        Entry<T> previous;
        Entry<T> next;
        Entry(T element) {
            this.element = element;
            this.previous = null;
            this.next = null;
        }
    }

    public StdHistory(int maxHeight){
        Contract.checkCondition(maxHeight>0);
        this.maxHeight = maxHeight;
        this.endPosition = 0;
        this.currentPosition = 0;
        this.currentEntry = null;
        this.firstEntry = null;
    }

    @Override
    public E getCurrentElement() {
        Contract.checkCondition(getCurrentPosition()>0);
        return this.currentEntry.element;
    }

    @Override
    public int getMaxHeight() {
        return this.maxHeight;
    }

    @Override
    public int getCurrentPosition() {
        return this.currentPosition;
    }

    @Override
    public int getEndPosition() {
        return this.endPosition;
    }

    @Override
    public void add(E e) {
        Contract.checkCondition(e != null);
        Entry<E> entry = new Entry<E>(e);
        if(this.currentPosition>0){
        	this.currentEntry.next = entry;
        } else {
        	this.firstEntry = entry;
        }
        if(this.currentPosition == this.maxHeight){
         	this.firstEntry = this.firstEntry.next;
        	this.firstEntry.previous = null;
        } else {
        	this.currentPosition++;
        }
        entry.previous = this.currentEntry;
        this.currentEntry = entry;
        this.endPosition = this.currentPosition;
    }

    @Override
    public void goForward() {
        Contract.checkCondition(this.currentPosition < this.endPosition);
        this.currentPosition++;
        this.currentEntry = this.currentEntry.next;
    }

    @Override
    public void goBackward() {
        Contract.checkCondition(this.currentPosition > 0);
        this.currentPosition--;
        this.currentEntry=this.currentEntry.previous;
    }

}
