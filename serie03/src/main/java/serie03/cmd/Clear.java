package serie03.cmd;

import serie03.Text;
import util.Contract;

import java.util.ArrayList;
import java.util.List;

public class Clear extends AbstractCommand {

    private List<String> backup;
    /**
     * @inv
     *     getBackup() != null
     * @cons
     *     $ARGS$ Text text
     *     $PRE$ text != null
     *     $POST$
     *         getText() == text
     *         getState() == State.DO
     *         getBackup().size() == 0
     */

    /**
     * @param text
     * @pre <pre>
     *     text != null </pre>
     * @post <pre>
     *     getText() == text
     *     getState() == State.DO </pre>
     */

    public Clear(Text text) {
        super(text);
        this.backup = new ArrayList<String>();
    }

    List<String> getBackup(){
        return new ArrayList<String>(backup);
    }

    @Override
    protected void doIt() {
        Contract.checkCondition(canDo());
        for (int i = 1; i <= getText().getLinesNb(); i++) {
           this.backup.add(getText().getLine(i));
        }
        getText().clear();
    }

    @Override
    protected void undoIt() {
        Contract.checkCondition(canUndo());
        getText().clear();
        for (int i = 1; i <= this.backup.size(); i++) {
            getText().insertLine(i,this.backup.get(i - 1));
        }
        this.backup.clear();
    }

}
