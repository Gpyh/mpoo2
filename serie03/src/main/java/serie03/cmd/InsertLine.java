package serie03.cmd;

import serie03.Text;
import util.Contract;

public class InsertLine extends AbstractCommand {

    private int index;
    private String line;
    /**
     * @inv
     *     1 <= getIndex()
     *     getLine() != null
     *     canDo() ==> getIndex() <= getText().getLinesNb() + 1
     *     canUndo() ==> getIndex() <= getText().getLinesNb()
     * @cons
     *     $ARGS$ Text text, int numLine, String line
     *     $PRE$
     *         text != null
     *         line != null
     *         1 <= numLine
     *     $POST$
     *         getText() == text
     *         getIndex() == numLine
     *         getLine().equals(line)
     *         getState() == State.DO
     */

    /**
     * @param text
     * @pre <pre>
     *     text != null </pre>
     * @post <pre>
     *     getText() == text
     *     getState() == State.DO </pre>
     */

    public InsertLine(Text text,int numLine, String line) {
        super(text);
        Contract.checkCondition(numLine > 0);
        Contract.checkCondition(line != null);
        this.index = numLine;
        this.line = line;
    }

    protected int getIndex(){
        return this.index;
    }

    protected String getLine(){
        return this.line;
    }

    @Override
    protected void doIt() {
        getText().insertLine(getIndex(),getLine());
        this.state = State.UNDO;
    }

    @Override
    protected void undoIt() {
        getText().deleteLine(getIndex());
        this.state = State.DO;
    }
}
